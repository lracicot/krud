<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $customerName = '';

    /**
     * @ORM\Column(type="string")
     */
    private $customerCity;

    /**
     * @ORM\Column(type="string")
     */
    private $customerAddress;

    /**
     * @ORM\Column(type="string")
     */
    private $billingPhone1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $billingPhone2;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $customerEmail;

    /**
     * @ORM\Column(type="boolean")
     */
    private $contacted;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invoice", mappedBy="customer")
     */
    private $invoices;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return static
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerName()
    {
      return $this->customerName;
    }

    /**
     * @param mixed $customerName
     *
     * @return static
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerCity()
    {
        return $this->customerCity;
    }

    /**
     * @param mixed $customerCity
     *
     * @return static
     */
    public function setCustomerCity($customerCity)
    {
        $this->customerCity = $customerCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddress()
    {
        return $this->customerAddress;
    }

    /**
     * @param mixed $customerAddress
     *
     * @return static
     */
    public function setCustomerAddress($customerAddress)
    {
        $this->customerAddress = $customerAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingPhone1()
    {
        return $this->billingPhone1;
    }

    /**
     * @param mixed $billingPhone1
     *
     * @return static
     */
    public function setBillingPhone1($billingPhone1)
    {
        $this->billingPhone1 = $billingPhone1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingPhone2()
    {
        return $this->billingPhone2;
    }

    /**
     * @param mixed $billingPhone2
     *
     * @return static
     */
    public function setBillingPhone2($billingPhone2)
    {
        $this->billingPhone2 = $billingPhone2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * @param mixed $invoices
     *
     * @return static
     */
    public function setInvoices($invoices)
    {
        $this->invoices = $invoices;
        return $this;
    }

    public function __toString()
    {
        return $this->customerName;
    }

    /**
     * @return mixed
     */
    public function getCustomerEmail()
    {
      return $this->customerEmail;
    }

    /**
     * @param mixed $customerEmail
     *
     * @return static
     */
    public function setCustomerEmail($customerEmail)
    {
      $this->customerEmail = $customerEmail;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getContacted()
    {
      return $this->contacted;
    }

    /**
     * @param mixed $contacted
     *
     * @return static
     */
    public function setContacted($contacted)
    {
      $this->contacted = $contacted;
      return $this;
    }
}
