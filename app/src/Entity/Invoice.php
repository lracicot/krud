<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 */
class Invoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paid;

    /**
     * @ORM\Column(type="text")
     */
    private $locationInfos;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $locationPhone1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $locationPhone2;

    /**
     * @ORM\Column(type="string")
     */
    private $authorizedBy;

    /**
     * @ORM\Column(type="string")
     */
    private $orderId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $invoiced;

    /**
     * @ORM\Column(type="string")
     */
    private $paymentMode;

    /**
     * @ORM\Column(type="string")
     */
    private $referedBy;

    /**
     * @ORM\Column(type="string")
     */
    private $truckId;

    /**
     * @ORM\Column(type="string")
     */
    private $operator;

    /**
     * @ORM\Column(type="boolean")
     */
    private $camera;

    /**
     * @ORM\Column(type="boolean")
     */
    private $rooter;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pressure;

    /**
     * @ORM\Column(type="boolean")
     */
    private $camera_localisation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $camera_inspection;

    /**
     * @ORM\Column(type="boolean")
     */
    private $depotoire;

    /**
     * @ORM\Column(type="boolean")
     */
    private $presence_racine;

    /**
     * @ORM\Column(type="boolean")
     */
    private $presence_graisse;

    /**
     * @ORM\Column(type="string")
     */
    private $presence_autre;

    /**
     * @ORM\Column(type="boolean")
     */
    private $nettoyage;

    /**
     * @ORM\Column(type="boolean")
     */
    private $debouchage;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deblocage;

    /**
     * @ORM\Column(type="boolean")
     */
    private $rahabilitation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inspection_pre;

    /**
     * @ORM\Column(type="boolean")
     */
    private $detection;

    /**
     * @ORM\Column(type="decimal")
     */
    private $gageBefore;

    /**
     * @ORM\Column(type="decimal")
     */
    private $gageAfter;

    /**
     * @ORM\Column(type="decimal")
     */
    private $liters;

    /**
     * @ORM\Column(type="time")
     */
    private $transportTime;

    /**
     * @ORM\Column(type="time")
     */
    private $workStart;

    /**
     * @ORM\Column(type="time")
     */
    private $workEnd;

    /**
     * @ORM\Column(type="time")
     */
    private $disposalStart;

    /**
     * @ORM\Column(type="time")
     */
    private $disposalEnd;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="invoices")
     * @ORM\JoinColumn(nullable=true, name="customer_id")
     */
    private $customer;

    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return static
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return static
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     *
     * @return static
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @param mixed $paid
     *
     * @return static
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingPhone1()
    {
        return $this->billingPhone1;
    }

    /**
     * @param mixed $billingPhone1
     *
     * @return static
     */
    public function setBillingPhone1($billingPhone1)
    {
        $this->billingPhone1 = $billingPhone1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingPhone2()
    {
        return $this->billingPhone2;
    }

    /**
     * @param mixed $billingPhone2
     *
     * @return static
     */
    public function setBillingPhone2($billingPhone2)
    {
        $this->billingPhone2 = $billingPhone2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocationInfos()
    {
        return $this->locationInfos;
    }

    /**
     * @param mixed $locationInfos
     *
     * @return static
     */
    public function setLocationInfos($locationInfos)
    {
        $this->locationInfos = $locationInfos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocationPhone1()
    {
        return $this->locationPhone1;
    }

    /**
     * @param mixed $locationPhone1
     *
     * @return static
     */
    public function setLocationPhone1($locationPhone1)
    {
        $this->locationPhone1 = $locationPhone1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocationPhone2()
    {
        return $this->locationPhone2;
    }

    /**
     * @param mixed $locationPhone2
     *
     * @return static
     */
    public function setLocationPhone2($locationPhone2)
    {
        $this->locationPhone2 = $locationPhone2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthorizedBy()
    {
        return $this->authorizedBy;
    }

    /**
     * @param mixed $authorizedBy
     *
     * @return static
     */
    public function setAuthorizedBy($authorizedBy)
    {
        $this->authorizedBy = $authorizedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     *
     * @return static
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoiced()
    {
        return $this->invoiced;
    }

    /**
     * @param mixed $invoiced
     *
     * @return static
     */
    public function setInvoiced($invoiced)
    {
        $this->invoiced = $invoiced;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferedBy()
    {
        return $this->referedBy;
    }

    /**
     * @param mixed $referedBy
     *
     * @return static
     */
    public function setReferedBy($referedBy)
    {
        $this->referedBy = $referedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTruckId()
    {
        return $this->truckId;
    }

    /**
     * @param mixed $truckId
     *
     * @return static
     */
    public function setTruckId($truckId)
    {
        $this->truckId = $truckId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param mixed $operator
     *
     * @return static
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCamera()
    {
        return $this->camera;
    }

    /**
     * @param mixed $camera
     *
     * @return static
     */
    public function setCamera($camera)
    {
        $this->camera = $camera;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRooter()
    {
        return $this->rooter;
    }

    /**
     * @param mixed $rooter
     *
     * @return static
     */
    public function setRooter($rooter)
    {
        $this->rooter = $rooter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVaccum()
    {
        return $this->vaccum;
    }

    /**
     * @param mixed $vaccum
     *
     * @return static
     */
    public function setVaccum($vaccum)
    {
        $this->vaccum = $vaccum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPressure()
    {
        return $this->pressure;
    }

    /**
     * @param mixed $pressure
     *
     * @return static
     */
    public function setPressure($pressure)
    {
        $this->pressure = $pressure;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGageBefore()
    {
        return $this->gageBefore;
    }

    /**
     * @param mixed $gageBefore
     *
     * @return static
     */
    public function setGageBefore($gageBefore)
    {
        $this->gageBefore = $gageBefore;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGageAfter()
    {
        return $this->gageAfter;
    }

    /**
     * @param mixed $gageAfter
     *
     * @return static
     */
    public function setGageAfter($gageAfter)
    {
        $this->gageAfter = $gageAfter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLiters()
    {
        return $this->liters;
    }

    /**
     * @param mixed $liters
     *
     * @return static
     */
    public function setLiters($liters)
    {
        $this->liters = $liters;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransportTime()
    {
        return $this->transportTime;
    }

    /**
     * @param mixed $transportTime
     *
     * @return static
     */
    public function setTransportTime($transportTime)
    {
        $this->transportTime = $transportTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkStart()
    {
        return $this->workStart;
    }

    /**
     * @param mixed $workStart
     *
     * @return static
     */
    public function setWorkStart($workStart)
    {
        $this->workStart = $workStart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkEnd()
    {
        return $this->workEnd;
    }

    /**
     * @param mixed $workEnd
     *
     * @return static
     */
    public function setWorkEnd($workEnd)
    {
        $this->workEnd = $workEnd;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisposalStart()
    {
        return $this->disposalStart;
    }

    /**
     * @param mixed $disposalStart
     *
     * @return static
     */
    public function setDisposalStart($disposalStart)
    {
        $this->disposalStart = $disposalStart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisposalEnd()
    {
        return $this->disposalEnd;
    }

    /**
     * @param mixed $disposalEnd
     *
     * @return static
     */
    public function setDisposalEnd($disposalEnd)
    {
        $this->disposalEnd = $disposalEnd;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
      return $this->total;
    }

    /**
     * @param mixed $total
     *
     * @return static
     */
    public function setTotal($total)
    {
      $this->total = $total;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerName()
    {
      return $this->customerName;
    }

    /**
     * @param mixed $customerName
     *
     * @return static
     */
    public function setCustomerName($customerName)
    {
      $this->customerName = $customerName;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerCity()
    {
      return $this->customerCity;
    }

    /**
     * @param mixed $customerCity
     *
     * @return static
     */
    public function setCustomerCity($customerCity)
    {
      $this->customerCity = $customerCity;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddress()
    {
        return $this->customerAddress;
    }

    /**
     * @param mixed $customerAddress
     *
     * @return static
     */
    public function setCustomerAddress($customerAddress)
    {
        $this->customerAddress = $customerAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMode()
    {
      return $this->paymentMode;
    }

    /**
     * @param mixed $paymentMode
     *
     * @return static
     */
    public function setPaymentMode($paymentMode)
    {
        $this->paymentMode = $paymentMode;
        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getCameraLocalisation()
    {
      return $this->camera_localisation;
    }

    /**
     * @param mixed $camera_localisation
     *
     * @return static
     */
    public function setCameraLocalisation($camera_localisation)
    {
      $this->camera_localisation = $camera_localisation;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getCameraInspection()
    {
      return $this->camera_inspection;
    }

    /**
     * @param mixed $camera_inspection
     *
     * @return static
     */
    public function setCameraInspection($camera_inspection)
    {
      $this->camera_inspection = $camera_inspection;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getDepotoire()
    {
      return $this->depotoire;
    }

    /**
     * @param mixed $depotoire
     *
     * @return static
     */
    public function setDepotoire($depotoire)
    {
      $this->depotoire = $depotoire;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getPresenceRacine()
    {
      return $this->presence_racine;
    }

    /**
     * @param mixed $presence_racine
     *
     * @return static
     */
    public function setPresenceRacine($presence_racine)
    {
      $this->presence_racine = $presence_racine;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getPresenceGraisse()
    {
      return $this->presence_graisse;
    }

    /**
     * @param mixed $presence_graisse
     *
     * @return static
     */
    public function setPresenceGraisse($presence_graisse)
    {
      $this->presence_graisse = $presence_graisse;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getPresenceAutre()
    {
      return $this->presence_autre;
    }

    /**
     * @param mixed $presence_autre
     *
     * @return static
     */
    public function setPresenceAutre($presence_autre)
    {
      $this->presence_autre = $presence_autre;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getNettoyage()
    {
      return $this->nettoyage;
    }

    /**
     * @param mixed $nettoyage
     *
     * @return static
     */
    public function setNettoyage($nettoyage)
    {
      $this->nettoyage = $nettoyage;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getDebouchage()
    {
      return $this->debouchage;
    }

    /**
     * @param mixed $debouchage
     *
     * @return static
     */
    public function setDebouchage($debouchage)
    {
      $this->debouchage = $debouchage;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getDeblocage()
    {
      return $this->deblocage;
    }

    /**
     * @param mixed $deblocage
     *
     * @return static
     */
    public function setDeblocage($deblocage)
    {
      $this->deblocage = $deblocage;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getRahabilitation()
    {
      return $this->rahabilitation;
    }

    /**
     * @param mixed $rahabilitation
     *
     * @return static
     */
    public function setRahabilitation($rahabilitation)
    {
      $this->rahabilitation = $rahabilitation;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getInspectionPre()
    {
      return $this->inspection_pre;
    }

    /**
     * @param mixed $inspection_pre
     *
     * @return static
     */
    public function setInspectionPre($inspection_pre)
    {
      $this->inspection_pre = $inspection_pre;
      return $this;
    }

    /**
     * @return mixed
     */
    public function getDetection()
    {
      return $this->detection;
    }

    /**
     * @param mixed $detection
     *
     * @return static
     */
    public function setDetection($detection)
    {
      $this->detection = $detection;
      return $this;
    }
}
