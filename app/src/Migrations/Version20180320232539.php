<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180320232539 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, customer_name VARCHAR(255) NOT NULL, customer_city VARCHAR(255) NOT NULL, customer_address VARCHAR(255) NOT NULL, billing_phone1 VARCHAR(255) NOT NULL, billing_phone2 VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE invoice DROP customer_name, DROP customer_city, DROP customer_address, DROP billing_phone1, DROP billing_phone2');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE customer');
        $this->addSql('ALTER TABLE invoice ADD customer_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD customer_city VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD customer_address VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD billing_phone1 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD billing_phone2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
