<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180321211847 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer ADD customer_email VARCHAR(255) DEFAULT NULL, ADD contacted TINYINT(1) NOT NULL, CHANGE billing_phone2 billing_phone2 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE invoice ADD camera_inspection TINYINT(1) NOT NULL, ADD depotoire TINYINT(1) NOT NULL, ADD presence_racine TINYINT(1) NOT NULL, ADD presence_graisse TINYINT(1) NOT NULL, ADD presence_autre VARCHAR(255) NOT NULL, ADD nettoyage TINYINT(1) NOT NULL, ADD debouchage TINYINT(1) NOT NULL, ADD deblocage TINYINT(1) NOT NULL, ADD rahabilitation TINYINT(1) NOT NULL, ADD inspection_pre TINYINT(1) NOT NULL, ADD detection TINYINT(1) NOT NULL, CHANGE location_phone1 location_phone1 VARCHAR(255) DEFAULT NULL, CHANGE location_phone2 location_phone2 VARCHAR(255) DEFAULT NULL, CHANGE invoiced invoiced VARCHAR(255) DEFAULT NULL, CHANGE vaccum camera_localisation TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer DROP customer_email, DROP contacted, CHANGE billing_phone2 billing_phone2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE invoice ADD vaccum TINYINT(1) NOT NULL, DROP camera_localisation, DROP camera_inspection, DROP depotoire, DROP presence_racine, DROP presence_graisse, DROP presence_autre, DROP nettoyage, DROP debouchage, DROP deblocage, DROP rahabilitation, DROP inspection_pre, DROP detection, CHANGE location_phone1 location_phone1 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE location_phone2 location_phone2 VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE invoiced invoiced VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
