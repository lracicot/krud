/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS krud;
CREATE DATABASE krud;
USE krud;

# Dump of table invoice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoice`;
DROP TABLE IF EXISTS `customer`;

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `location_infos` longtext COLLATE utf8_unicode_ci NOT NULL,
  `location_phone1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_phone2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorized_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoiced` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `refered_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `operator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `camera` tinyint(1) NOT NULL,
  `rooter` tinyint(1) NOT NULL,
  `camera_localisation` tinyint(1) NOT NULL,
  `pressure` tinyint(1) NOT NULL,
  `gage_before` decimal(10,0) NOT NULL,
  `gage_after` decimal(10,0) NOT NULL,
  `liters` decimal(10,0) NOT NULL,
  `transport_time` time NOT NULL,
  `work_start` time NOT NULL,
  `work_end` time NOT NULL,
  `disposal_start` time NOT NULL,
  `disposal_end` time NOT NULL,
  `total` double NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `camera_inspection` tinyint(1) NOT NULL,
  `depotoire` tinyint(1) NOT NULL,
  `presence_racine` tinyint(1) NOT NULL,
  `presence_graisse` tinyint(1) NOT NULL,
  `presence_autre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nettoyage` tinyint(1) NOT NULL,
  `debouchage` tinyint(1) NOT NULL,
  `deblocage` tinyint(1) NOT NULL,
  `rahabilitation` tinyint(1) NOT NULL,
  `inspection_pre` tinyint(1) NOT NULL,
  `detection` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_906517449395C3F3` (`customer_id`),
  CONSTRAINT `FK_906517449395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_phone2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
